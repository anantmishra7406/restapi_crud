
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Subject")
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;	  
	private int age;
	
	@ElementCollection(targetClass=String.class)
	private List<String> skill;
	 
	public Employee() {
		
	}
	      
	public Integer getId() {
		return id;
	}

	public Employee(Integer id, String name, int age , String skills  , List<String> skill) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		 
		this.skill = skill;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
 
	public List<String> getSkill() {
		return skill;
	}

	public void setSkill(List<String> skill) {
		this.skill = skill;
	}
 

}
